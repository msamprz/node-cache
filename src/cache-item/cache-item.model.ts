import { Document, Model, model, Schema } from 'mongoose';
import AppConfig from '../../config/app-config';

/**
 * Interface to model the CacheItem Schema for TypeScript.
 * @param key:string
 * @param data:string
 * @param iat:Date
 * @param exp:Date
 */
export interface ICacheItem extends Document {
  key: string;
  data: string;
  iat: Date;
  exp: Date;
}

const cacheItemSchema: Schema = new Schema({
  key: {
    type: String,
    required: true,
    unique: true
  },
  data: {
    type: String,
    required: true
  },
  iat: {
    type: Date,
    default: Date.now
  },
  exp: {
    type: Date,
    default: () => (Date.now() + (AppConfig.TTL_POLICY * 1000)),
  }
});

const CacheItem: Model<ICacheItem> = model('CacheItem', cacheItemSchema);

export default CacheItem;