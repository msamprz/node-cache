import { Router } from 'express';
import HttpStatusCodes from 'http-status-codes';

import CacheItem from '../cache-item/cache-item.model';

const router: Router = Router();

router.get('/', async (_req, res) => {
  try {
    const all = await CacheItem.find();
    return res.json(all.map(item => item.key));
  } catch (err) {
    console.error(err.message);

    return res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Server Error' });
  }
});

router.delete('/', async (_req, res) => {
  try {
    await CacheItem.deleteMany({});

    return res.status(HttpStatusCodes.NO_CONTENT).end();
  } catch (err) {
    console.error(err.message);

    return res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Server Error' });
  }
});

export default router;