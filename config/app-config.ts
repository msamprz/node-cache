const AppConfig = Object.freeze({
  MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost/cached',
  PORT: parseInt(process.env.NODE_PORT) || 3000,
  TTL_POLICY: parseInt(process.env.TTL_POLICY) || 3600, // 1 * 60 * 60
  ITEM_LIMIT_POLICY: parseInt(process.env.ITEM_LIMIT_POLICY) || 10,
});

export default AppConfig;
