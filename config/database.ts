import { ConnectionOptions, connect } from 'mongoose';
import Config from './app-config';

const connectDB = async () => {
  try {
    const mongoURI: string = Config.MONGO_URI;
    const options: ConnectionOptions = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false
    };

    await connect(
      mongoURI,
      options
    );

    console.log('Connected to DB.');
  } catch (err) {
    console.error(err.message);
    process.exit(1);
  }
};

export default connectDB;
