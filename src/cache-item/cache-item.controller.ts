import { Router } from 'express';
import HttpStatusCodes from 'http-status-codes';
import { check, validationResult } from 'express-validator/check';

import CacheItem, { ICacheItem } from './cache-item.model';
import { fetchDataFromSource } from './cache-item.service';
import AppConfig from '../../config/app-config';

const router: Router = Router();

router.get('/:key', async (_req, res) => {
  // TODO: Change console logs to file logs
  try {
    let cacheItem: ICacheItem = await CacheItem.findOne({ key: _req.params.key });

    if (cacheItem && cacheItem.exp.getTime() > Date.now()) {
      console.log('Cache hit');

      await cacheItem.update({ exp: (Date.now() + (AppConfig.TTL_POLICY * 1000)) });

      return res.json(cacheItem.data);
    } else {
      console.log('Cache miss');
      const cacheItemFields = {
        key: _req.params.key,
        data: fetchDataFromSource(),
      };

      cacheItem = new CacheItem(cacheItemFields);

      await cacheItem.save();

      return res.json(cacheItem.data);
    }
  } catch (err) {
    console.error(err.message);

    return res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Server Error' });
  }
});

router.post('/', [
  check('key', 'Key is required')
    .not()
    .isEmpty(),
  check('data', 'Data is required')
    .not()
    .isEmpty(),
], async (_req: any, res: any) => {
  const errors = validationResult(_req);

  if (!errors.isEmpty()) {
    return res.status(HttpStatusCodes.BAD_REQUEST).json({ errors: errors.array() });
  }

  try {
    const currentItemCount = await CacheItem.estimatedDocumentCount();

    if (currentItemCount >= AppConfig.ITEM_LIMIT_POLICY) {
      // This is a very crude implementation of the cache limit.
      // It does not block additions in any way
      // and the condition is very basic, which only checks
      // for cache items that have already been invalidated
      // and removes all.
      // It also doesn't use an exact document count, instead
      // it uses an estimate based off of the latest DB index.
      await CacheItem.deleteMany({
        iat: {
          $lte: (Date.now() - (AppConfig.TTL_POLICY * 1000))
        }
      });
    }

    const cacheItem = new CacheItem({ key: _req.body.key, data: _req.body.data });

    await cacheItem.save();

    return res.json(cacheItem);
  } catch (err) {
    console.error(err.message);

    return res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Server Error' });
  }
});

router.patch('/:key', [
  check('data', 'Data is required')
    .not()
    .isEmpty(),
], async (_req: any, res: any) => {
  const errors = validationResult(_req);

  if (!errors.isEmpty()) {
    return res.status(HttpStatusCodes.BAD_REQUEST).json({ errors: errors.array() });
  }

  const { key } = _req.params;
  const { data } = _req.body;

  try {
    const cacheItem = await CacheItem.findOneAndUpdate(
      { key },
      { data, exp: (Date.now() + (AppConfig.TTL_POLICY * 1000)) },
      { new: true }
    );

    return res.json(cacheItem);
  } catch (err) {
    console.error(err.message);

    return res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Server Error' });
  }
});

router.delete('/:key', async (_req, res) => {
  const { key } = _req.params;

  try {
    await CacheItem.findOneAndRemove({ key });

    return res.status(HttpStatusCodes.NO_CONTENT).end();
  } catch (err) {
    console.error(err.message);

    return res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Server Error' });
  }
});

export default router;