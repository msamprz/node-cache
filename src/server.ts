import bodyParser from 'body-parser';
import express from 'express';

import connectDB from '../config/database';
import AppConfig from '../config/app-config';
import CacheItem from './cache-item/cache-item.controller';
import Keys from './keys/keys.controller';

const app = express();

// Connect to MongoDB
connectDB();

// Express configuration
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (_req, res) => {
  return res.json(`API running`);
});

// Module path declaration
app.use('/key', CacheItem);
app.use('/keys', Keys);

const server = app.listen(AppConfig.PORT, () =>
  console.log(`Server started on port ${AppConfig.PORT}`)
);

export default server;
