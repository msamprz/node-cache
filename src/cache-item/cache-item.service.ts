import { make } from 'sentencer';

export const fetchDataFromSource = () => {
    return make('I like collecting {{ noun }}.');
};
